<!--
@Author: shumez
@Date:   2018-04-09 19:31:14
@Project: PyML
@Filename: CHANGELOG.md
@Last modified by:   shumez
@Last modified time: 2018-05-25 11:40:33
-->


# ChangeLog


## [Unreleased]


## [0.1.1] - 18-05-28

### Added
- add pyprind (Python Progress Indicator)

```
18-05-28 10:50:35
  ~
 pip install pyprind
Collecting pyprind
  Downloading https://files.pythonhosted.org/packages/1e/30/e76fb0c45da8aef49ea8d2a90d4e7a6877b45894c25f12fb961f009a891e/PyPrind-2.11.2-py3-none-any.whl
Installing collected packages: pyprind
Successfully installed pyprind-2.11.2

```

## [0.1.0] - 2018-05-25

### Changed
- change ```*/img/``` hard link


## [0.0.1] - 2018-04-09

### Added
- initialized PyML





[Unreleased]: 
[0.0.1]: .
[0.1.0]: .
[0.1.1]: .
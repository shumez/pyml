<!--
@Author: shumez
@Date:   2018-04-09 19:30:55
@Project: PyML
@Filename: README.md
@Last modified by:   shumez
@Last modified time: 2018-04-28 14:21:77
-->


PyML
====

<!--
Python Machine Learning 2e:
<small>Unlock modern machine learning and deep learning techniques with Python by using the latest cutting-edge open source Python</small>
-->

```
TY  - BOOK
T1  - Python Machine Learning
A1  - Raschka, S.
A1  - Mirjalili, V.
SN  - 9781787126022
UR  - https://books.google.co.jp/books?id=_plGDwAAQBAJ
Y1  - 2017
PB  - Packt Publishing
```
![cover](COVER.jpg)

Files
-----

```
PyML
├── CHANGELOG.md
├── COVER.jpg
├── Icon\r
├── README.md
├── c01
│   ├── cahier.md
│   ├── code
│   │   └── ch01.ipynb
│   ├── img
│   │   ├── 01_01.png
│   │   ├── 01_02.png
│   │   ├── 01_03.png
│   │   ├── 01_04.png
│   │   ├── 01_05.png
│   │   ├── 01_06.png
│   │   ├── 01_07.png
│   │   ├── 01_08.png
│   │   ├── 01_09.png
│   │   ├── ipynb_ex1.png
│   │   └── ipynb_ex2.png
│   └── note.ipynb
├── c02
│   ├── README.md
│   ├── code
│   │   ├── adaline_gd.py
│   │   ├── adaline_sgd.py
│   │   ├── ch02.ipynb
│   │   ├── perceptron.py
│   │   └── plot_decision_regions.py
│   ├── img
│   │   ├── 02-to-address.txt
│   │   ├── 02_01.png
│   │   ├── 02_02.graffle
│   │   ├── 02_02.png
│   │   ├── 02_03.graffle
│   │   ├── 02_03.png
│   │   ├── 02_04.png
│   │   ├── 02_05.png
│   │   ├── 02_06.png
│   │   ├── 02_07.png
│   │   ├── 02_08.png
│   │   ├── 02_09.png
│   │   ├── 02_10.png
│   │   ├── 02_11.png
│   │   ├── 02_12.png
│   │   ├── 02_13.png
│   │   ├── 02_14.png
│   │   ├── 02_14_1.png
│   │   ├── 02_14_2.png
│   │   ├── 02_15.png
│   │   ├── 02_15_1.png
│   │   ├── 02_15_2.png
│   │   ├── 3547_02_images.pdf
│   │   ├── 3547_02_images.pptx
│   │   ├── _02_15.png
│   │   ├── _02_16.png
│   │   ├── jupyter-example-1.png
│   │   └── jupyter-example-2.png
│   ├── nb.md
│   └── note.ipynb
├── c03
│   ├── CHANGELOG.md
│   ├── README.md
│   ├── code
│   │   ├── ch03.ipynb
│   │   └── logistic_regression_gd.py
│   ├── img
│   │   ├── 03_01.png
│   │   ├── 03_02.png
│   │   ├── 03_03.png
│   │   ├── 03_04.png
│   │   ├── 03_05.png
│   │   ├── 03_06.png
│   │   ├── 03_07.png
│   │   ├── 03_08.png
│   │   ├── 03_09.png
│   │   ├── 03_10.png
│   │   ├── 03_11.png
│   │   ├── 03_12.png
│   │   ├── 03_13.png
│   │   ├── 03_14.png
│   │   ├── 03_15.png
│   │   ├── 03_16.png
│   │   ├── 03_17.png
│   │   ├── 03_18.png
│   │   ├── 03_19.png
│   │   ├── 03_20.png
│   │   ├── 03_21.png
│   │   ├── 03_22.png
│   │   ├── 03_23.png
│   │   ├── 03_24.png
│   │   └── tree.png
│   └── note.ipynb
├── c04
│   ├── README.md
│   ├── code
│   │   ├── ch04.ipynb
│   │   ├── decision_tree.py
│   │   ├── knn.py
│   │   ├── linear_svm.py
│   │   ├── random_forest.py
│   │   ├── rbf_svm.py
│   │   └── sbs.py
│   ├── img
│   │   ├── 04_01.png
│   │   ├── 04_02.png
│   │   ├── 04_03.png
│   │   ├── 04_04.png
│   │   ├── 04_05.png
│   │   ├── 04_06.png
│   │   ├── 04_07.png
│   │   ├── 04_08.png
│   │   └── 04_09.png
│   └── note.ipynb
├── c05
│   ├── code
│   │   ├── ch05.ipynb
│   │   ├── plot_decision_regions.py
│   │   ├── rbf_kernel_pca.py
│   │   └── rbf_kernel_pca_2.py
│   ├── data
│   │   └── wine.data
│   ├── img
│   │   ├── 05_01.png
│   │   ├── 05_02.png
│   │   ├── 05_03.png
│   │   ├── 05_04.png
│   │   ├── 05_05.png
│   │   ├── 05_06.png
│   │   ├── 05_07.png
│   │   ├── 05_08.png
│   │   ├── 05_09.png
│   │   ├── 05_10.png
│   │   ├── 05_11.png
│   │   ├── 05_12.png
│   │   ├── 05_13.png
│   │   ├── 05_14.png
│   │   ├── 05_15.png
│   │   ├── 05_16.png
│   │   ├── 05_17.png
│   │   ├── 05_18.png
│   │   └── 05_19.png
│   └── note.ipynb
└── c06
    ├── code
    │   └── ch06.ipynb
    ├── data
    │   └── wdbc.data
    ├── img
    │   ├── 06_01.png
    │   ├── 06_02.png
    │   ├── 06_03.png
    │   ├── 06_04.png
    │   ├── 06_05.png
    │   ├── 06_06.png
    │   ├── 06_07.png
    │   ├── 06_08.png
    │   ├── 06_09.png
    │   └── 06_10.png
    └── note.ipynb
```


Content
-------

- [c01](c01/note.ipynb)
    - ...
- [c02](c02/note.ipynb)
    - [2.1 Artificial neurons - a brief glimpse into the early history of machine learning](#2.1-Artificial-neurons---a-brief-glimpse-into-the-early-history-of-machine-learning)
        - [2.1.1 The formal definition of an artificial neuron](#2.1.1-The-formal-definition-of-an-artificial-neuron)
        - [2.1.2 The perceptron learning rule](#2.1.2-The-perceptron-learning-rule)
    - [2.2 Implementing a perceptron learning algorithm in Python](#2.2-Implementing-a-perceptron-learning-algorithm-in-Python)
        - [2.2.1 Object-oriented perceptron API](#2.2.1-Object-oriented-perceptron-API)
    - [2.3 Training a perceptron model on Iris dataset](#2.3-Training-a-perceptron-model-on-Iris-dataset)
    - [2.4 Adaptive linear neurons (ADALINE) & the convergence of learning](#2.4-Adaptive-linear-neurons-(ADALINE)-&-the-convergence-of-learning)
    - [2.5 Minimizing cost fn w/ gradient descent](#2.5-Minimizing-cost-fn-w/-gradient-descent)
        - [2.5.1 Implementing ADALINE in Python](#2.5.1-Implementing-ADALINE-in-Python)
        - [2.5.2 Improving gradient descent through feature scaling](#2.5.2-Improving-gradient-descent-through-feature-scaling)
    - [2.6 Large scale machine learning & stochastic gradient descent](#2.6-Large-scale-machine-learning-&-stochastic-gradient-descent)
- [c03](c03/note.ipynb)
    - [3.1 Chosing a classification algorithm](#3.1-Chosing-a-classification-algorithm)
    - [3.2 First steps w/ scikit-learn](#3.2-First-steps-w/-scikit-learn)
    - [3.3 Modeling class probabilisties via logistic regression](#3.3-Modeling-class-probabilisties-via-logistic-regression)
        - [3.3.1 Logistic regression intuition & conditional probabilities](#3.3.1-Logistic-regression-intuition-&-conditional-probabilities)
        - [3.3.2 Learning the weights of the logistic cost function](#3.3.2-Learning-the-weights-of-the-logistic-cost-function)
        - [3.3.3](#3.3.3)
        - [3.3.4 Training a logistic regression model w/ scikit-learn](#3.3.4-Training-a-logistic-regression-model-w/-scikit-learn)
        - [3.3.5 Tacking overfitting via regularization](#3.3.5-Tacking-overfitting-via-regularization)
    - [3.4 Maximum margin classifcation w/ support vector machines](#3.4-Maximum-margin-classifcation-w/-support-vector-machines)
        - [3.4.1 Maximum margin intuition](#3.4.1-Maximum-margin-intuition)
        - [3.4.2 Dealing w/ the nonlinearly separable case using slack variables](#3.4.2-Dealing-w/-the-nonlinearly-separable-case-using-slack-variables)
    - [3.5 Solving non-linear problems using a kernel SVM](#3.5-Solving-non-linear-problems-using-a-kernel-SVM)
        - [3.5.1](#3.5.1)
        - [3.5.2 Using the kernel trick to find separating hyperplanes in higher dimensional space](#3.5.2-Using-the-kernel-trick-to-find-separating-hyperplanes-in-higher-dimensional-space)
    - [3.6 Decision tree learning](#3.6-Decision-tree-learning)
        - [3.6.1 Maximizing information gain - getting the most bang for the buck](#3.6.1-Maximizing-information-gain-getting-the-most-bang-for-the-buck)
        - [3.6.2 Building a decision tree](#3.6.2-Building-a-decision-tree)
        - [3.6.3 Combining weak to strong learners via random forests](#3.6.3-Combining-weak-to-strong-learners-via-random-forests)
    - [3.7 K-nearest neighbors - a lazy learning algorithm](#3.7-K-nearest-neighbors---a-lazy-learning-algorithm)
- [c04](c04/note.ipynb)
    - [4.1 Dealing w/ missing data](c04/note.ipynb#4.1-Dealing-w/-missing-data)
        - [4.1.1 Identifying missing values in tabular data](c04/note.ipynb#4.1.1-Identifying-missing-values-in-tabular-data)
        - [4.1.2 Eliminating samples / features w/ missing values](c04/note.ipynb#4.1.2-Eliminating-samples-/-features-w/-missing-values)
        - [4.1.3 Imputing missing val](c04/note.ipynb#4.1.3-Imputing-missing-val)
        - [4.1.4 Understanding the scikit-learn estimator API](c04/note.ipynb#4.1.4-Understanding-the-scikit-learn-estimator-API)
    - [4.2 Handling categorical data](c04/note.ipynb#4.2-Handling-categorical-data)
        - [4.2.1 Nominal & ordinal features](c04/note.ipynb#4.2.1-Nominal-&-ordinal-features)
        - [4.2.2 Mapping ordinal feat](c04/note.ipynb#4.2.2-Mapping-ordinal-feat)
        - [4.2.3 Encoding class labels](c04/note.ipynb#4.2.3-Encoding-class-labels)

- [c05](c05/note.ipynb)
    - [5.1 Unsupervised dimensionality reduction via principal component analysis](#5.1-Unsupervised-dimensionality-reduction-via-principal-component-analysis)
        - [5.1.1 The main steps behind principal component analysis](#5.1.1-The-main-steps-behind-principal-component-analysis)
        - [5.1.2 Extracting the principal components step-by-step](#5.1.2-Extracting-the-principal-components-step-by-step)
        - [5.1.3 Total and explained variance](#5.1.3-Total-and-explained-variance)
        - [5.1.4 Feature transformation](#5.1.4-Feature-transformation)
        - [5.1.5 Principal component analysis in scikit-learn](#5.1.5-Principal-component-analysis-in-scikit-learn)
    - [5.2 Supervised data component via linear discriminant analysis](#5.2-Supervised-data-component-via-linear-discriminant-analysis)
        - [5.2.1 Principal component analysis versus linear disciminant analysis](#5.2.1-Principal-component-analysis-versus-linear-disciminant-analysis)
        - [5.2.2 The inner working of linear discriminant analysis](#5.2.2-The-inner-working-of-linear-discriminant-analysis)
        - [5.2.3 Computing the scatter matrices](#5.2.3-Computing-the-scatter-matrices)
        - [5.2.4 Selecting linear discriminats for the new feature subspace](#5.2.4-Selecting-linear-discriminats-for-the-new-feature-subspace)
        - [5.2.5 Projecting samples onto the new feature space](#5.2.5-Projecting-samples-onto-the-new-feature-space)
        - [5.2.6 LDA via scikit-learn](#5.2.6-LDA-via-scikit-learn)
    - [5.3 Using kernel principal component analysis for nonlinear mappings](#5.3-Using-kernel-principal-component-analysis-for-nonlinear-mappings)
        - [5.3.1 Implementing a kernel principal component analysis in Python](#5.3.1-Implementing-a-kernel-principal-component-analysis-in-Python)
        - [5.3.2 Implementing a kernel principal component analysis in Python](#5.3.2-Implementing-a-kernel-principal-component-analysis-in-Python)
        - [5.3.3 Projecting new data points](#5.3.3-Projecting-new-data-points)
        - [5.3.4 Kernel principal component analysis in scilit-learn](#5.3.4-Kernel-principal-component-analysis-in-scilit-learn)

- [c06](c06/note.ipynb)
    - [6.1 Streamlining workflows with piplines](#6.1-Streamlining-workflows-with-piplines)
        - [6.1.1 Loading the Breast Cancer Wisconsin dataset](#6.1.1-Loading-the-Breast-Cancer-Wisconsin-dataset)
        - [6.1.2 Combining transformers and estimators in a pipline](#6.1.2-Combining-transformers-and-estimators-in-a-pipline)
    - [6.2 Using k-fold cross validation to assess model preformance](#6.2-Using-k-fold-cross-validation-to-assess-model-preformance)
        - [6.2.1 The holdout method](#6.2.1-The-holdout-method)
        - [6.2.2 K-fold cross-validation](#6.2.2-K-fold-cross-validation)
    - [6.3 Debugging algorithms with learning curves](#6.3-Debugging-algorithms-with-learning-curves)
        - [6.3.1 Diagnosing bias and variance problems with learning curves](#6.3.1-Diagnosing-bias-and-variance-problems-with-learning-curves)
        - [6.3.2 Addressing over and underfitting with validation curves](#6.3.2-Addressing-over-and-underfitting-with-validation-curves)
    - [6.4 Fine-tuning machine learning models via grid search](#6.4-Fine-tuning-machine-learning-models-via-grid-search)
        - [6.4.1 Tuning hyperparameters via grid search](#6.4.1-Tuning-hyperparameters-via-grid-search)
        - [6.4.2 Algorithm selection with nested cross-validation](#6.4.2-Algorithm-selection-with-nested-cross-validation)
    - [6.5 Looking at different performance evaluation metrics](#6.5-Looking-at-different-performance-evaluation-metrics)
        - [6.5.1 Reading a confusion matrix](#6.5.1-Reading-a-confusion-matrix)
        - [6.5.2 Optimizing the precision and recall of a classification model](#6.5.2-Optimizing-the-precision-and-recall-of-a-classification-model)
        - [6.5.3 Plotting a receiver operating characteristic](#6.5.3-Plotting-a-receiver-operating-characteristic)
        - [6.5.4 The scoring metrics for multiclass classification](#6.5.4-The-scoring-metrics-for-multiclass-classification)
    - [6.6 Dealing with class imbalance](#6.6-Dealing-with-class-imbalance)

- [c07](c07/note.ipynb)
    - [7.1 Learning with ensembles](#7.1-Learning-with-ensembles)


Resources
---------

- [@rasbt/python-machine-learning-book-2nd-edition]



[@rasbt/python-machine-learning-book-2nd-edition]: https://github.com/rasbt/python-machine-learning-book-2nd-edition

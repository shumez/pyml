<!--
@Author: shumez
@Date:   2018-04-17 10:33:39
@Project: PyML/c02
@Filename: README.md
@Last modified by:   shumez
@Last modified time: 2018-04-17 10:33:50
-->


# PyML/c02


## Files

```
├── ch02.ipynb
├── code
│   ├── adaline_gd.py
│   ├── adaline_sgd.py
│   └── perceptron.py
├── img
│   ├── 02-to-address.txt
│   ├── 02_01.png
│   ├── 02_02.graffle
│   ├── 02_02.png
│   ├── 02_03.graffle
│   ├── 02_03.png
│   ├── 02_04.png
│   ├── 02_05.png
│   ├── 02_06.png
│   ├── 02_07.png
│   ├── 02_08.png
│   ├── 02_09.png
│   ├── 02_10.png
│   ├── 02_11.png
│   ├── 02_12.png
│   ├── 02_13.png
│   ├── 02_14.png
│   ├── 02_14_1.png
│   ├── 02_14_2.png
│   ├── 02_15.png
│   ├── 02_15_1.png
│   ├── 02_15_2.png
│   ├── 3547_02_images.pdf
│   ├── 3547_02_images.pptx
│   ├── _02_15.png
│   ├── _02_16.png
│   ├── jupyter-example-1.png
│   └── jupyter-example-2.png
├── nb.md
└── note.ipynb
```


## Resources

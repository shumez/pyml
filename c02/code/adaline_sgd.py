# AdalineSGD
from numpy.random import seed

class AdalineSGD(object):
    """
    ADAptive LInear NEuron classifier

    Parameters
    ----------
    eta: float
        learning rate (0.0 <, ≤ 1.0)
    n_iter: int
        # traing
    shuffle: bool (default: True)
        True: shuffle training data every epoch
    random_state: int
        random num generator for random weight initialization

    Attributes
    ----------
    w_: 1d-array
        weight after fitting
    cost_: list
        sum-of-squres
    """

    def __init__(self, eta=0.01, n_iter=10, shuffle=True, random_state=None):
        # init learning rate
        self.eta = eta

        # init n_iter
        self.n_iter = n_iter

        # weight 初期化フラグ False
        self.w_initialized = False

        # shuffle フラグ
        self.shuffle = shuffle

        # random num seed
        self.random_state = random_state

    def fit(self, X, y):
        """
        fit training data

        Parameters
        ----------
        X: {array-like}, shape = [n_samples, n_features]
            training data
            n_features: # of features

        y: array-like, shape = [n_samples]
            target var

        Returns
        -------
        self: object
        """

        # weight
        self._initialize_weights(X.shape[1])

        # cost
        self.cost_ = []

        # traingin data
        for i in range(self.n_iter):
            # training data shuffle
            if self.shuffle:
                X, y = self._shuffle(X, y)

            # list
            cost = []

            # sample
            for xi, target in zip(X, y):
                # feature xi, target y
                cost.append(self._update_weights(xi, target))

            # mean cost for sample
            avg_cost = sum(cost) / len(y)

            #
            self.cost_.append(avg_cost)

        return self

    def partial_fit(self, X, y):
        """
        fit to training data
        """

        # 初期化されていない場合は初期化
        if not self.w_initialized:
            self._initialized_weights(X.shape[1])

        # target var y
        # sample features xi, target var でweight を更新
        if y.ravel().shape[0] > 1:
            for xi, target in zip(X, y):
                self._update_weights(xi, target)

        # target y
        # sample features X, target y
        else:
            self._update_weights(X, y)
        return self

    def _shuffle(self, X, y):
        """
        shuffle training data
        """

        r = self.rgen.permutation(len(y))
        return X[r], y[r]

    def _initialize_weights(self, m):
        """
        weight を小さなrandom numに初期化
        """

        self.rgen = np.random.RandomState(self.random_state)
        self.w_ = self.rgen.normal(loc=0.0, scale=0.01, size=1 + m)
        self.w_initialized = True

    def _update_weights(self, xi, target):
        """
        ADALINE の学習規則を用いてweightを更新
        """

        # activation fn のoutput
        output = self.activation(self.net_input(xi))

        # error
        error = (target - output)

        # weight w1, ... , wm の更新
        self.w_[1:] += self.eta * xi.dot(error)

        # weight w0 の更新
        self.w_[0] += self.eta * error

        # cost
        cost = 0.5 * error ** 2

        return cost

    def net_input(self, X):
        """
        net input
        """

        return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
        """
        linear activation fn
        """
        return X

    def predict(self, X):
        """
        1 step
        """

        return np.where(self.activation(self.net_input(X)) >= 0.0, 1, -1)

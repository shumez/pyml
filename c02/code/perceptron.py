# perceptron
import numpy as np

class Perceptron(object):

    """
    classifier

    parameter
    ---------
    eta: float
        learning rate
    n_iter: int
        training number
    random_state: int
        random num seed

    attribute
    ---------
    w_: array
       weight
    errors_: list
        num of errors
    """

    def __init__(self, eta=0.01, n_iter=50, random_state=1):
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state

    def fit(self, X, y):
        """
        fit to training data

        paramter
        --------
        X: {array}, shape = [n_samples, n_features]
            training data
            n_samples: # of samples
            n_features: # of features

        y: array-like, shape = [n_samples]
            response var

        return
        ------
        self: object
        """

        rgen = np.random.RandomState(self.random_state)
        self.w_ = rgen.normal(loc=0.0, scale=0.01, size=1 + X.shape[1])
        self.errors_ = []

        for _ in range(self.n_iter):
            errors = 0

            for xi, target in zip(X, y):
                # weight w1, ...  , wm
                # Δwj
                update = self.eta * (target - self.predict(xi))
                self.w_[1:] += update * xi

                # update weight (w0): Δw0 = η(y(i) - y^(i))
                self.w_[0] += update

                #
                errors += int(update != 0.0)
            #
            self.errors_.append(errors)
        return self

    def net_input(self, X):
        """calculate net input"""
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def predict(self, X):
        """
        + 1step, return class level
        """
        return np.where(self.net_input(X) >= 0.0, 1, -1)

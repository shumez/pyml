<!--
@Author: shumez
@Date:   2018-04-10 17:11:56
@Project: proj
@Filename: nb.md
@Last modified by:   shumez
@Last modified time: 2018-04-10 17:12:19
-->


# 2 Classification

- Parceptron
- ADALINE (Adaptive Linear Neuron)

## 2.1


### 2.1.1

- **decision func** (*Φ(z)*)
- **input** (*x*)
- **weight** (*w*)
- **net input** (*z*)
- **threshold** (*θ*)

**unit step func**


\[
\phi(z) = 1 (z ≥ 0), -1 (z < 0)
\]


<!--
*Φ(z)* = 1 (*z* ≥ 0), -1 (*z* < 0)
-->

\[
z = w
\]

*z* = *w^T^ x*
(= *w0x0* + *w1x1* + *wmxm* )
(= Σx~j~w~j~ )

<!--
@Author: shumez
@Date:   2018-04-17 11:37:36
@Project: PyML/c03
@Filename: README.md
@Last modified by:   shumez
@Last modified time: 2018-04-17 13:42:53
-->


# c03

## Files

```
c03
├── CHANGELOG.md
├── README.md
├── code
│   └── ch03.ipynb
├── img
│   ├── 03_01.png
│   ├── 03_02.png
│   ├── 03_03.png
│   ├── 03_04.png
│   ├── 03_05.png
│   ├── 03_06.png
│   ├── 03_07.png
│   ├── 03_08.png
│   ├── 03_09.png
│   ├── 03_10.png
│   ├── 03_11.png
│   ├── 03_12.png
│   ├── 03_13.png
│   ├── 03_14.png
│   ├── 03_15.png
│   ├── 03_16.png
│   ├── 03_17.png
│   ├── 03_18.png
│   ├── 03_19.png
│   ├── 03_20.png
│   ├── 03_21.png
│   ├── 03_22.png
│   ├── 03_23.png
│   └── 03_24.png
└── note.ipynb
```

Resources
---------

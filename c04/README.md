<!--
@Author: shumez
@Date:   2018-04-28 13:44:03
@Project: c04
@Filename: README.md
@Last modified by:   shumez
@Last modified time: 2018-04-28 13:45:83
-->


# c04


## Files

```
c04
├── README.md
├── code
│   └── ch04.ipynb
├── img
│   ├── 04_01.png
│   ├── 04_02.png
│   ├── 04_03.png
│   ├── 04_04.png
│   ├── 04_05.png
│   ├── 04_06.png
│   ├── 04_07.png
│   ├── 04_08.png
│   └── 04_09.png
└── note.ipynb
```

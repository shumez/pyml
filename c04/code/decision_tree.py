# decision tree

from sklearn.tree import DecisionTreeClassifier

# Entropy を index をする DT の instance を生成
tree = DecisionTreeClassifier(criterion='gini', max_depth=4, random_state=1)

# fit to training data
tree.fit(X_train, y_train)
X_combined = np.vstack((X_train, X_test))
y_combined = np.hstack((y_train, y_test))

# plot
plot_decision_regions(X_combined, y_combined, classifier=tree,
                     test_idx=range(105, 150))

plt.xlabel('petal length [cm]')
plt.ylabel('petal width [cm]')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

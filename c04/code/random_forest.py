# random forest

from sklearn.ensemble import RandomForestClassifier

# entropy index

forest = RandomForestClassifier(criterion='gini',
                                n_estimators=25, random_state=1, n_jobs=2)

# fit to training data
forest.fit(X_train, y_train)

# plot
plot_decision_regions(X_combined, y_combined, classifier=forest,
                     test_idx=range(105, 150))

plt.xlabel('petal length [cm]')
plt.ylabel('petal width [cm]')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

# rbf svm

# RBF kernel のよる SVM の instance を生成 (2つのparameter を変更)
svm = SVC(kernel='rbf', random_state=1, gamma=0.2, C=1.0)

svm.fit(X_train_std, y_train)

# plot
plot_decision_regions(X_combined_std, y_combined, classifier=svm,
                     test_idx=range(105, 150))
plt.xlabel('petal length [standardized]')
plt.ylabel('petal width [standardized ]')
plt.legend(loc='upper left')
plt.tight_layout()
plt.show()

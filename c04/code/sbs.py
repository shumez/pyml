# sequential backward selection

from sklearn.base import clone
from itertools import combinations
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score

class SBS():
    """
    Sequential Backward Selection
    """

    def __init__(self, estimator, k_features, scoring=accuracy_score,
                test_size=0.25, random_state=1):
        # feat を評価する index
        self.scoring = scoring
        # estimator
        self.estimator = clone(estimator)
        # 選択する feat の個数
        self.k_features = k_features
        # test data の割合
        self.test_size = test_size
        # random state seed を固定
        self.random_state = random_state

    def fit(self, X, y):
        # split training, test data
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=self.test_size,
                                                           random_state=self.random_state)
        # すべての feat の個数, col index
        dim = X_train.shape[1]
        self.indices_ = tuple(range(dim))
        self.subsets_ = [self.indices_]

        # すべての feat を用いて, score を算出
        score = self._calc_score(
            X_train, y_train,
            X_test, y_test, self.indices_)

        # score
        self.scores_ = [score]

        #
        while dim > self.k_features:
            # emplty list
            scores = []
            subsets = []

            # feat の部分集合を表すcol ind を組み合わせごとに, 処理を反復
            for p in combinations(self.indices_, r=dim - 1):

                # score を算出して格納
                score = self._calc_score (X_train, y_train, X_test, y_test, p)
                scores.append(score)

                # feat の部分集合を表す col ind の list を格納
                subsets.append(p)

            # 最良の score の ind を抽出
            best = np.argmax(scores)
            # 最良の score となる col ind を抽出して格納
            self.indices_ = subsets[best]
            self.subsets_.append(self.indices_)

            # feat の個数を1つだけ減らして, 次のstepへ
            dim -= 1

            # score を格納
            self.scores_.append(scores[best])

        # 最後に格納した score
        self.k_score_ = self.scores_[-1]

        return self

    def transform(self, X):
        # 抽出した feat を返す
        return X[:, self.indices_]

    def _calc_score(self, X_train, y_train, X_test, y_test, indices):
        # 指定された col num indices の feat を抽出して model を適合
        self.estimator.fit(X_train[:, indices], y_train)
        # test data を用いて, class label を予測
        y_pred = self.estimator.predict(X_test[:, indices])
        # 真の class label を予測値を用いて score を算出
        score =self.scoring (y_test, y_pred)
        return score

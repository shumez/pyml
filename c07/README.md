<!--
@Author: shumez
@Date:   2018-05-25 13:27:55
@Project: PyML/c07
@Filename: README.md
@Last modified by:   shumez
@Last modified time: 2018-05-25 13:28:02
-->


# Chapter 07

[![fig](img/07_01.png)](img/07_01.png)
[![fig](img/07_02.png)](img/07_02.png)
[![fig](img/07_03.png)](img/07_03.png)
[![fig](img/07_04.png)](img/07_04.png)
[![fig](img/07_05.png)](img/07_05.png)
[![fig](img/07_06.png)](img/07_06.png)
[![fig](img/07_07.png)](img/07_07.png)
[![fig](img/07_08.png)](img/07_08.png)
[![fig](img/07_09.png)](img/07_09.png)
[![fig](img/07_10.png)](img/07_10.png)
[![fig](img/07_11.png)](img/07_11.png)



[DOC.md]

[docs]

## Description

```
├── README.md
├── code
│   ├── ch07.ipynb
│   └── majority_vote_classifier.py
├── data
│   ├── wine.data
│   └── wine.names.txt
├── img
│   ├── 07_01.png
│   ├── 07_02.png
│   ├── 07_03.png
│   ├── 07_04.png
│   ├── 07_05.png
│   ├── 07_06.png
│   ├── 07_07.png
│   ├── 07_08.png
│   ├── 07_09.png
│   ├── 07_10.png
│   └── 07_11.png
└── note.ipynb
```

## Getting Started



### Dependencies



### Installing



### Executing program

```
```

## Help

```
```

## Authors

* [shumez]

## Version History

[CHANGELOG.md]

## License

[LICENSE.md]


## Acknowledgements


<!-- ------------------------------- -->
[shumez]: shumez
[DOC.md]: DOC.md
[docs]: docs/
[CHANGELOG.md]: CHANGELOG.md
[LICENSE.md]: LICENSE.md

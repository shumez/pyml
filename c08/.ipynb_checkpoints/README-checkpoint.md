<!--
@Author: shumez
@Date:   2018-06-02 10:20:08
@Project: proj
@Filename: README.md
@Last modified by:   shumez
@Last modified time: 2018-06-02 10:20:12
-->


# c08


[DOC.md]

[docs]

## Description

```
c08
├── CHANGELOG.md
├── LICENSE.md
├── README.md
├── code
│   └── ch08.ipynb
├── data
│   ├── aclImdb
│   │   ├── README
│   │   ├── imdb.vocab
│   │   ├── imdbEr.txt
│   │   ├── test
│   │   └── train
│   ├── aclImdb_v1.tar
│   ├── aclImdb_v1.tar.gz
│   ├── movie_data.csv
│   └── movie_data.csv.gz
├── img
└── note.ipynb
```
## Getting Started



### Dependencies



### Installing



### Executing program

```
```

## Help

```
```

## Authors

* [shumez]

## Version History

[CHANGELOG.md]

## License

[LICENSE.md]


## Acknowledgements


<!-- ------------------------------- -->
[shumez]: shumez
[DOC.md]: DOC.md
[docs]: docs/
[CHANGELOG.md]: CHANGELOG.md
[LICENSE.md]: LICENSE.md

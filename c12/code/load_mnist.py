# load MNIST

import os 
import struct
import numpy as np

def load_mnist(path, kind='train'):
    """
    load MNIST data from `path`
    """

    labels_path = os.path.join(path, '%s-labels-idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images-idx1-ubyte' % kind)

    # read file
    with open(labels_path, 'rb') as lbpath:
    	# binary -> str
    	magic, n = struct.unpack('>II', lbpath.read(8))

    	labels = np.fromfile(lbpath, dtype=np.unit8)

    with open(images_path, 'rb') as imgpath:
    	magic, num, rows, cols = struct.unpack('>III', imgpath.read(16))

    	images = np.fromfile(imgpath, dtype=np.unit8).reshape(len(labels), 784)

    	images = ((images / 255.) - .5) * 2

    return images, labels